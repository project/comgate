<?php declare(strict_types=1);

namespace Drupal\comgate;

use Drupal\comgate\Dto\Payment;
use Drupal\comgate\Dto\Request\PaymentCreateRequest;
use Drupal\comgate\Dto\Request\PaymentStatusRequest;
use Drupal\comgate\Dto\Request\TransferListRequest;
use Drupal\comgate\Dto\Response\JsonResponse;
use Drupal\comgate\Dto\Response\Response;
use Drupal\comgate\Dto\Transaction;
use Drupal\comgate\Http\ITransport;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Comgate API client.
 */
class ComgateClient {

  protected ITransport $transport;

  public function __construct(ITransport $transport) {
    $this->transport = $transport;
  }

  /**
   * Creates a Comgate payment.
   */
  public function createPayment(Payment $payment): Response {
    return $this->transport->post('create', PaymentCreateRequest::of($payment)->toArray());
  }

  /**
   * Returns a status of a given Comgate payment.
   */
  public function getStatus(Transaction $transaction): Response {
    return $this->transport->post('status', PaymentStatusRequest::of($transaction)->toArray());
  }

  /**
   * Obtains information about which transfers were made within a given day.
   */
  public function listTransfers(DrupalDateTime $date): JsonResponse {
    return $this->transport->postForJson('transferList', TransferListRequest::of($date)->toArray());
  }

}

<?php declare(strict_types=1);

namespace Drupal\comgate;

use Drupal\user\UserFloodControlInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides simple IP based flood control mechanism.
 */
trait FloodControlTrait {

  protected UserFloodControlInterface $floodControl;

  /**
   * Enforces flood control for the current request.
   *
   * @param string $name
   *   The name of an event.
   * @param int $threshold
   *   The maximum number of times each user can do this event per time window.
   * @param int $window
   *   (optional) Number of seconds in the time window for this event (default is 3600
   *   seconds, or 1 hour).
   */
  protected function floodControl(string $name, int $threshold, int $window = 3600): void {
    // Protect this resource with max 20 attempts/hour.
    if (!$this->floodControl->isAllowed($name, $threshold, $window)) {
      throw new AccessDeniedHttpException('Access is blocked because of IP based flood prevention.', NULL, Response::HTTP_TOO_MANY_REQUESTS);
    }
    $this->floodControl->register($name);
  }

}

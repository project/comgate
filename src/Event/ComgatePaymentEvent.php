<?php declare(strict_types=1);

namespace Drupal\comgate\Event;

use Drupal\comgate\Dto\Payment;
use Drupal\Component\EventDispatcher\Event;

/**
 * Defines the payment event.
 *
 * @see \Drupal\comgate\Event\ComgateEvents
 */
class ComgatePaymentEvent extends Event {

  /**
   * The payment status.
   */
  protected Payment $payment;

  /**
   * Constructs a new ComgatePaymentStatusEvent.
   *
   * @param \Drupal\comgate\Dto\Payment $payment
   */
  public function __construct(Payment $payment) {
    $this->payment = $payment;
  }

  /**
   * Gets the payment.
   *
   * @return \Drupal\comgate\Dto\Payment
   *   The payment.
   */
  public function getPayment(): Payment {
    return $this->payment;
  }

}

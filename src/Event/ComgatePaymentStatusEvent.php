<?php declare(strict_types=1);

namespace Drupal\comgate\Event;

use Drupal\comgate\Dto\PaymentStatus;
use Drupal\Component\EventDispatcher\Event;

/**
 * Defines the payment status event.
 *
 * @see \Drupal\comgate\Event\ComgateEvents
 */
class ComgatePaymentStatusEvent extends Event {

  /**
   * The payment status.
   */
  protected PaymentStatus $paymentStatus;

  /**
   * Constructs a new ComgatePaymentStatusEvent.
   *
   * @param \Drupal\comgate\Dto\PaymentStatus $paymentStatus
   *   The payment status.
   */
  public function __construct(PaymentStatus $paymentStatus) {
    $this->paymentStatus = $paymentStatus;
  }

  /**
   * Gets the payment.
   *
   * @return \Drupal\comgate\Dto\PaymentStatus
   *   The payment status.
   */
  public function getStatus(): PaymentStatus {
    return $this->paymentStatus;
  }

}

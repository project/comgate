<?php declare(strict_types=1);

namespace Drupal\comgate\Event;

final class ComgateEvents {

  /**
   * Name of the event fired after a payment is created.
   *
   * @Event
   *
   * @see \Drupal\comgate\Event\ComgatePaymentEvent
   */
  const PAYMENT_CREATED = 'comgate.payment_created';

  /**
   * Name of the event fired after a payment result is received.
   *
   * @Event
   *
   * @see \Drupal\comgate\Event\PaymentStatusReceived
   */
  const PAYMENT_STATUS_RECEIVED = 'comgate.payment_status_received';
}

<?php declare(strict_types=1);

namespace Drupal\comgate\Plugin\Block;

use Drupal\comgate\Dto\Payment;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Provides a block with a test Comgate payment.
 *
 * @Block(
 *   id = "comgate_test_payment",
 *   admin_label = @Translation("Comgate test payment"),
 *   category = @Translation("Comgate"),
 * )
 */
class TestPaymentButton extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    /** @var ConfigFactoryInterface $config_factory */
    $config_factory = \Drupal::getContainer()->get('config.factory');
    $site_mail = $config_factory->get('system.site')->get('mail');

    $payment = Payment::create()
      ->withPrice(12.34)
      ->withCurrency('CZK')
      ->withMethod('ALL')
      ->withCountry('CZ')
      ->withLabel('Test')
      ->withReferenceId('000001')
      ->withEmail($site_mail);

    $form = \Drupal::formBuilder()->getForm('Drupal\comgate\Form\PaymentForm', $payment);
    return [
      '#markup' => $this->t('Total: @sum', ['@sum' => '12.34 CZK']),
      'form' => $form,
    ];
  }

}

<?php declare(strict_types=1);

namespace Drupal\comgate;

use Drupal\comgate\Dto\Payment;
use Drupal\comgate\Dto\PaymentRedirect;
use Drupal\comgate\Dto\PaymentStatus;
use Drupal\comgate\Dto\Response\ResponseBase;
use Drupal\comgate\Dto\Transaction;
use Drupal\comgate\Event\ComgateEvents;
use Drupal\comgate\Event\ComgatePaymentEvent;
use Drupal\comgate\Event\ComgatePaymentStatusEvent;
use Drupal\comgate\Exception\Runtime\ComgateException;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Drupal service for Comgate integration.
 */
class ComgateService {

  use LoggerAwareTrait;
  use MessengerTrait;

  protected ComgateClient $client;

  protected TransactionLog $transactionLog;

  protected EventDispatcherInterface $eventDispatcher;

  /**
   * ComgateService constructor.
   */
  public function __construct(TransactionLog $transactionLog,
                              EventDispatcherInterface $eventDispatcher,
                              LoggerInterface $logger,
                              MessengerInterface $messenger) {
    $this->client = Comgate::defaults()->createClient();
    $this->transactionLog = $transactionLog;
    $this->eventDispatcher = $eventDispatcher;
    $this->logger = $logger;
    $this->messenger = $messenger;
  }

  /**
   * Creates a Comgate payment.
   *
   * @param \Drupal\comgate\Dto\Payment $payment
   *   The payment to create.
   *
   * @return \Drupal\comgate\Dto\PaymentRedirect
   *   The payment ID along with a redirect to redirect the customer to.
   */
  public function createPayment(Payment $payment): PaymentRedirect {
    // Create (prepare) a Comgate payment.
    $response = $this->client->createPayment($payment);
    if ($response->isError()) {
      $this->logComgateError($response);
      throw new ComgateException(sprintf('Request failed (error %d)', $response->getCode()));
    }

    $paymentRedirect = PaymentRedirect::createFrom($response->getData());
    $payment->withTransactionId($paymentRedirect->getTransactionId());

    // Remember it's ID to validate it later in other methods.
    $this->transactionLog->remember($paymentRedirect->getTransactionId());

    $this->eventDispatcher->dispatch(new ComgatePaymentEvent($payment), ComgateEvents::PAYMENT_CREATED);

    return $paymentRedirect;
  }

  /**
   * Handles payment result request incoming from Comgate.
   *
   * Here we expect that the incoming request has already been validated.
   *
   * @param array $data
   *   The attributes bag from the request.
   */
  public function handlePaymentResult(array $data): void {
    $status = PaymentStatus::createFrom($data);

    // Check if this is a legitimate request for a known payment.
    if (!$this->transactionLog->canRecall($status->getTransactionId())) {
      throw new ComgateException(
        sprintf('Transaction ID \'%s\' is not known.', Xss::filter($status->getTransactionId()))
      );
    }

    $this->eventDispatcher->dispatch(new ComgatePaymentStatusEvent($status), ComgateEvents::PAYMENT_STATUS_RECEIVED);
  }

  /**
   * Returns a status of a given Comgate payment.
   *
   * @param \Drupal\comgate\Dto\Transaction $transaction
   *   The transaction we want to get the status of.
   *
   * @return \Drupal\comgate\Dto\PaymentStatus
   *   The status of the given transaction.
   */
  public function getPaymentStatus(Transaction $transaction): PaymentStatus {
    $response = $this->client->getStatus($transaction);
    if ($response->isError()) {
      $this->logComgateError($response);
      throw new ComgateException(sprintf('Request failed (error %d)', $response->getCode()));
    }

    return PaymentStatus::createFrom($response->getData());
  }

  /**
   * Returns an URL leading to payment terminal for a given transaction.
   *
   * @param string $transactionId
   *   The transaction ID of the payment.
   *
   * @return string
   *   The URL to Comgate payment terminal.
   */
  public function getPaymentUrl(string $transactionId): string {
    return 'https://payments.comgate.cz/status/' . $transactionId;
  }

  /**
   * Logs a given error response.
   */
  protected function logComgateError(ResponseBase $response): void {
    $this->logger->error('Got error response from comgate: error %code - %message', [
      '%code' => $response->getCode(),
      '%message' => $response->getMessage(),
    ]);
  }

}

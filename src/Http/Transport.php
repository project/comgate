<?php declare(strict_types=1);

namespace Drupal\comgate\Http;

use Drupal\comgate\Config;
use Drupal\comgate\Dto\Response\JsonResponse;
use Drupal\comgate\Dto\Response\Response;
use Drupal\comgate\Exception\Runtime\ComgateException;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

/**
 * Guzzle implementation of Comgate transport.
 */
class Transport implements ITransport {

  protected ClientInterface $client;

  protected Config $config;

  public function __construct(ClientInterface $client, Config $config) {
    $this->client = $client;
    $this->config = $config;
  }

  public function get(string $uri, array $query, array $options = []): Response {
    $query = array_merge([
      'merchant' => $this->config->getMerchant(),
      'secret' => $this->config->getSecret(),
      'test' => $this->config->isTest() ? 'true' : 'false',
    ], $query);

    $options = array_merge($options, [
      'query' => $query,
    ]);

    try {
      $res = $this->client->request('GET', $uri, $options);
      return new Response($res);
    } catch (GuzzleException $e) {
      throw new ComgateException('Request failed', 0, $e);
    }
  }

  public function post(string $uri, array $data, array $options = []): Response {
    $res = $this->postRaw($uri, $data, $options);
    return new Response($res);
  }

  public function postForJson(string $uri, array $data, array $options = []): JsonResponse {
    $res = $this->postRaw($uri, $data, $options);
    return new JsonResponse($res);
  }

  protected function postRaw(string $uri, array $data, array $options = []): ResponseInterface {
    $data = array_merge([
      'merchant' => $this->config->getMerchant(),
      'secret' => $this->config->getSecret(),
      'test' => $this->config->isTest() ? 'true' : 'false',
    ], $data);

    $options = array_merge($options, [
      'form_params' => $data,
    ]);

    try {
      return $this->client->request('POST', $uri, $options);
    } catch (GuzzleException $e) {
      throw new ComgateException('Request failed', 0, $e);
    }
  }

}

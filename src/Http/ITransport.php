<?php declare(strict_types=1);

namespace Drupal\comgate\Http;

use Drupal\comgate\Dto\Response\JsonResponse;
use Drupal\comgate\Dto\Response\Response;

/**
 * API contract for HTTP transport.
 */
interface ITransport {

  /**
   * @param string $uri
   * @param array $query
   * @param array $options
   *
   * @return \Drupal\comgate\Dto\Response\Response
   */
  public function get(string $uri, array $query, array $options = []): Response;

  /**
   * @param string $uri
   * @param array $data
   * @param array $options
   *
   * @return \Drupal\comgate\Dto\Response\Response
   */
  public function post(string $uri, array $data, array $options = []): Response;

  /**
   * @param string $uri
   * @param array $data
   * @param array $options
   *
   * @return \Drupal\comgate\Dto\Response\JsonResponse
   */
  public function postForJson(string $uri, array $data, array $options = []): JsonResponse;

}

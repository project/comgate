<?php declare(strict_types=1);

namespace Drupal\comgate\Dto;

/**
 * Comgate Bank Transfer DTO.
 */
class BankTransfer {

  protected ?int $transferId;

  protected ?string $transferDate;

  protected ?string $accountCounterparty;

  protected ?string $accountOutgoing;

  protected ?string $variableSymbol;

  protected function __construct() {
  }

  public static function create(): self {
    return new static();
  }

  public static function createFrom(array $data): self {
    $self = new static();
    $self->transferId = $data['transferId'] ?? NULL;
    $self->transferDate = $data['transferDate'] ?? NULL;
    $self->accountCounterparty = $data['accountCounterparty'] ?? NULL;
    $self->accountOutgoing = $data['accountOutgoing'] ?? NULL;
    $self->variableSymbol = $data['variableSymbol'] ?? NULL;

    return $self;
  }

  public function getTransferId(): ?int {
    return $this->transferId;
  }

  public function getTransferDate(): ?string {
    return $this->transferDate;
  }

  public function getAccountCounterparty(): ?string {
    return $this->accountCounterparty;
  }

  public function getAccountOutgoing(): ?string {
    return $this->accountOutgoing;
  }

  public function getVariableSymbol(): ?string {
    return $this->variableSymbol;
  }

}

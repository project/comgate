<?php declare(strict_types=1);


namespace Drupal\comgate\Dto;

/**
 * Comgate Transaction DTO.
 */
class Transaction {

  /**
   * Unique alphanumeric identifier (code) of the transaction,
   * which will be displayed to the Payer at various stages of payment.
   */
  protected ?string $transactionId;

  protected function __construct() {
  }

  public static function create(): self {
    return new static();
  }

  public static function existing(string $transactionId): self {
    return (new static())->withTransactionId($transactionId);
  }

  public static function createFrom(array $data): self {
    $self = new static();
    $self->transactionId = $data['transId'] ?? NULL;

    return $self;
  }

  public function getTransactionId(): ?string {
    return $this->transactionId;
  }

  public function withTransactionId(string $transactionId): self {
    $this->transactionId = $transactionId;

    return $this;
  }

}

<?php declare(strict_types=1);

namespace Drupal\comgate\Dto\Codes;

/**
 * Enumeration of ISO 4217 currency codes supported by Comgate.
 */
final class CurrencyCode {

  public const CZK = 'CZK';

  public const EUR = 'EUR';

  public const PLN = 'PLN';

  public const HUF = 'HUF';

  public const USD = 'USD';

  public const GBP = 'GBP';

  public const RON = 'RON';

  public const HRK = 'HRK';

  public const NOK = 'NOK';

  public const SEK = 'SEK';

  public const SELF = [
    self::CZK,
    self::EUR,
    self::PLN,
    self::HUF,
    self::USD,
    self::GBP,
    self::RON,
    self::HRK,
    self::NOK,
    self::SEK,
  ];

}

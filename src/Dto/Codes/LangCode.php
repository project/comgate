<?php declare(strict_types=1);

namespace Drupal\comgate\Dto\Codes;

/**
 * Enumeration of ISO-2 language codes supported by Comgate.
 */
final class LangCode {

  public const CS = 'cs';

  public const SK = 'sk';

  public const EN = 'en';

  public const PL = 'pl';

  public const FR = 'fr';

  public const RO = 'ro';

  public const DE = 'de';

  public const HU = 'hu';

  public const SI = 'si';

  public const HR = 'hr';

  public const SELF = [
    self::CS,
    self::SK,
    self::EN,
    self::PL,
    self::FR,
    self::RO,
    self::DE,
    self::HU,
    self::SI,
    self::HR,
  ];

}

<?php declare(strict_types=1);

namespace Drupal\comgate\Dto\Codes;

/**
 * Enumeration of payment status codes supported by Comgate.
 */
final class PaymentStatusCode {

  /**
   * The payment has been created, the final result of the payment is not yet known.
   */
  public const PENDING = 'PENDING';

  /**
   * The payer successfully paid the payment - it is possible to issue the goods,
   * respectively. make the service available.
   */
  public const PAID = 'PAID';

  /**
   * Payment has not been paid, the goods will not be issued, resp. the service will not be provided.
   * In exceptional cases, this state may change to a PAID state.
   */
  public const CANCELLED = 'CANCELLED';

  /**
   * The pre-authorization of the payment was successful (the money on the payer's card was blocked).
   * We are waiting for another request to be confirmed or cancelled.
   */
  public const AUTHORIZED = 'AUTHORIZED';

  public const SELF = [
    self::PENDING,
    self::PAID,
    self::CANCELLED,
    self::AUTHORIZED,
  ];

}

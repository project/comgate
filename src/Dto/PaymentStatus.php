<?php declare(strict_types=1);

namespace Drupal\comgate\Dto;

/**
 * Comgate payment status DTO.
 */
class PaymentStatus extends Transaction {

  /**
   * E-shop identifier in the Comgate system
   */
  protected ?string $merchant;

  /**
   * A value of "true" means that the payment was created as a test, a value of "false" means a production version.
   */
  protected ?bool $test;

  /**
   * Price for a product in cents or pennies
   *
   * Must be in minimum of 1 CZK; 0,1 EUR; 1 PLN; 100 HUF; 1 USD; 1 GBP; 5 RON; 1 HRK; 0,5 NOK; 0,5 SEK.
   */
  protected ?Money $price;

  /**
   * Currency code according to ISO 4217. Available currencies: CZK, EUR, PLN, HUF, USD, GBP, RON, HRK, NOK, SEK.
   */
  protected ?string $currency;

  /**
   * Short description of the product (1-16 characters).
   *
   * This item enables to filter payments in the Client Portal.
   */
  protected ?string $label;

  /**
   * Parameter is suitable for entering a variable symbol or order number on the Client's side
   * (it does not have to be unique, ie it is possible to create more payments with the same refId).
   *
   * In the Client Portal and daily csv. the parameter is marked as Client ID.
   */
  protected ?string $referenceId;

  /**
   * Contact email on the Payer (for the purposes of a possible complaint).
   */
  protected ?string $email;

  /**
   * Current transaction status, allowed values:
   *
   * - "PAID" - payment was successfully paid
   * - "CANCELED" - the payment was not completed correctly and is cancelled
   * - "AUTHORIZED" - the requested pre-authorization was successful
   */
  protected ?string $status;

  /**
   * If the e-shop has set up an automatic deduction of the payment fee,
   * the transaction fee will be calculated in this field,
   * otherwise, the field will take the value "unknown"
   */
  protected ?string $fee;

  private function __construct() {
    parent::__construct();
  }

  public static function createFrom(array $data): self {
    $self = new static();
    $self->merchant = $data['merchant'] ?? NULL;
    $self->test = filter_var($data['test'] ?? NULL, FILTER_VALIDATE_BOOLEAN);
    $self->price = isset($data['price']) ? Money::ofCents((int) $data['price']) : NULL;
    $self->currency = $data['curr'] ?? NULL;
    $self->label = $data['label'] ?? NULL;
    $self->referenceId = $data['refId'] ?? NULL;
    $self->email = $data['email'] ?? NULL;
    $self->transactionId = $data['transId'] ?? NULL;
    $self->status = $data['status'] ?? NULL;
    $self->fee = $data['fee'] ?? NULL;

    return $self;
  }

  public function getMerchant(): ?string {
    return $this->merchant;
  }

  public function isTest(): ?bool {
    return $this->test;
  }

  public function getPrice(): ?Money {
    return $this->price;
  }

  public function getCurrency(): ?string {
    return $this->currency;
  }

  public function getLabel(): ?string {
    return $this->label;
  }

  public function getReferenceId(): ?string {
    return $this->referenceId;
  }

  public function getEmail(): ?string {
    return $this->email;
  }

  public function getStatus(): ?string {
    return $this->status;
  }

  public function getFee(): ?string {
    return $this->fee;
  }

}

<?php declare(strict_types=1);

namespace Drupal\comgate\Dto\Request;

use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Transfer list request envelope.
 */
class TransferListRequest {

  private DrupalDateTime $date;

  private function __construct(DrupalDateTime $date) {
    $this->date = $date;
  }

  public static function of(DrupalDateTime $date): self {
    return new static($date);
  }

  public function toArray(): array {
    return [
      'date' => $this->date->format('Y-m-d'),
    ];
  }

}

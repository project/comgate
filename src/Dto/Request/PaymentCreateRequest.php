<?php declare(strict_types=1);

namespace Drupal\comgate\Dto\Request;

use Drupal\comgate\Dto\Payment;
use Drupal\comgate\Exception\LogicalException;

/**
 * Payment creation request envelope.
 */
class PaymentCreateRequest {

  private Payment $payment;

  private function __construct(Payment $payment) {
    $this->payment = $payment;
  }

  public static function of(Payment $payment): self {
    return new static($payment);
  }

  public function toArray(): array {
    // Required

    $output = [
      'price' => $this->payment->getPrice()->get(), // in cents 10.25 => 1025
      'curr' => $this->payment->getCurrency(),
      'label' => $this->payment->getLabel(),
      'refId' => $this->payment->getReferenceId(),
      'email' => $this->payment->getEmail(),
      'prepareOnly' => $this->payment->isPrepareOnly() ? 'true' : 'false',
      'method' => NULL,
    ];

    if ($this->payment->getAllowedMethods() === [] && $this->payment->getExcludedMethods() !== []) {
      throw new LogicalException('There must be at least one allowed method');
    }

    if ($this->payment->getAllowedMethods() !== []) {
      $output['method'] = implode('+', $this->payment->getAllowedMethods());
    }

    if ($this->payment->getExcludedMethods() !== []) {
      $output['method'] = ltrim($output['method'] . '-' . implode('-', $this->payment->getExcludedMethods()), '-');
    }

    // Optional

    if ($this->payment->getPhone() !== NULL) {
      $output['phone'] = $this->payment->getPhone();
    }

    if ($this->payment->getName() !== NULL) {
      $output['name'] = $this->payment->getName();
    }

    if ($this->payment->getCountry() !== NULL) {
      $output['country'] = $this->payment->getCountry();
    }

    if ($this->payment->getAccount() !== NULL) {
      $output['account'] = $this->payment->getAccount();
    }

    if ($this->payment->getLang() !== NULL) {
      $output['lang'] = $this->payment->getLang();
    }

    if ($this->payment->isPreauth() !== NULL) {
      $output['preauth'] = $this->payment->isPreauth() ? 'true' : 'false';
    }

    if ($this->payment->isInitRecurring() !== NULL) {
      if ($this->payment->isPrepareOnly() !== TRUE) {
        throw new LogicalException('Field initRecurring requires prepareOnly=true');
      }

      $output['initRecurring'] = $this->payment->isInitRecurring() ? 'true' : 'false';
    }

    if ($this->payment->isVerification() !== NULL) {
      $output['initRecurring'] = $this->payment->isVerification() ? 'true' : 'false';
    }

    if ($this->payment->isEmbedded() !== NULL) {
      $output['embedded'] = $this->payment->isEmbedded() ? 'true' : 'false';
    }

    if ($this->payment->isEetReport() !== NULL) {
      $output['eetReport'] = $this->payment->isEetReport() ? 'true' : 'false';
    }

    if ($this->payment->getEetData() !== []) {
      $output['eetData'] = $this->payment->getEetData();
    }

    return $output;
  }

}

<?php declare(strict_types=1);

namespace Drupal\comgate\Dto\Request;

use Drupal\comgate\Dto\Transaction;

/**
 * Payment status request envelope.
 */
class PaymentStatusRequest {

  private Transaction $transaction;

  private function __construct(Transaction $transaction) {
    $this->transaction = $transaction;
  }

  public static function of(Transaction $transaction): self {
    return new static($transaction);
  }

  public function toArray(): array {
    return [
      'transId' => $this->transaction->getTransactionId(),
    ];
  }

}

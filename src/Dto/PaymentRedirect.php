<?php declare(strict_types=1);

namespace Drupal\comgate\Dto;

/**
 * Comgate Payment redirect received based on a payment creation request.
 */
class PaymentRedirect extends Transaction {

  /**
   * URL of the page where the Payer is to be redirected to make the payment.
   */
  protected ?string $redirect = NULL;

  private function __construct() {
    parent::__construct();
  }

  public static function createFrom(array $data): self {
    $self = new static();
    $self->transactionId = $data['transId'] ?? NULL;
    $self->redirect = $data['redirect'] ?? NULL;

    return $self;
  }

  public function getUrl(): ?string {
    return $this->redirect;
  }

  public function withUrl(?string $redirect): self {
    $this->redirect = $redirect;

    return $this;
  }

}

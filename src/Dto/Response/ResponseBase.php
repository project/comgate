<?php declare(strict_types=1);

namespace Drupal\comgate\Dto\Response;

use Psr\Http\Message\ResponseInterface;

/**
 * Base class for Comgate responses.
 */
abstract class ResponseBase {

  protected ResponseInterface $origin;

  public function __construct(ResponseInterface $origin) {
    $this->origin = $origin;
  }

  public function getOrigin(): ResponseInterface {
    return $this->origin;
  }

  public function getStatusCode(): int {
    return $this->origin->getStatusCode();
  }

  public function getCode(): int {
    return $this->getStatusCode();
  }

  public function getMessage(): string {
    return $this->origin->getReasonPhrase();
  }

  public function isOk(): bool {
    return $this->getStatusCode() === 200;
  }

  public function isError(): bool {
    return !$this->isOk();
  }

}

<?php declare(strict_types=1);

namespace Drupal\comgate\Dto\Response;

/**
 * Comgate API response encoded in JSON.
 */
class JsonResponse extends ResponseBase {

  protected ?array $parsed = NULL;

  public function getData(): array {
    return $this->getParsedBody();
  }

  protected function getParsedBody(): array {
    if ($this->parsed === NULL) {
      $body = $this->origin->getBody();
      $body->rewind();

      $content = $body->getContents();

      $this->parsed = json_decode($content, TRUE);
    }

    return $this->parsed;
  }

}

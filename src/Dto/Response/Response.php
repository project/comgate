<?php declare(strict_types=1);

namespace Drupal\comgate\Dto\Response;

use GuzzleHttp\Psr7\Query;

/**
 * Comgate API response with envelope.
 */
class Response extends ResponseBase {

  protected ?array $parsed = NULL;

  public function isOk(): bool {
    return $this->getCode() === 0;
  }

  public function getCode(): int {
    return (int) ($this->getField('code') ?? -1);
  }

  public function getMessage(): string {
    return $this->getField('message') ?? 'N/A';
  }

  public function getData(): array {
    return $this->getParsedBody();
  }

  public function getField(string $key): mixed {
    return $this->getParsedBody()[$key] ?? NULL;
  }

  public function setParsedBody(array $parsed): void {
    $this->parsed = $parsed;
  }

  protected function getParsedBody(): array {
    if ($this->parsed === NULL) {
      $body = $this->origin->getBody();
      $body->rewind();

      $content = $body->getContents();

      $this->parsed = Query::parse($content);
    }

    return $this->parsed;
  }

}

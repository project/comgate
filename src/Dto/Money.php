<?php declare(strict_types=1);

namespace Drupal\comgate\Dto;

use Drupal\comgate\Exception\LogicalException;

/**
 * Comgate Money DTO.
 */
class Money {

  protected int $value;

  protected function __construct(int $value) {
    $this->value = $value;
  }

  public static function of(int|float|Money $money): self {
    if (is_int($money)) {
      return self::ofInt($money);
    }

    if (is_float($money)) {
      return self::ofFloat($money);
    }

    if ($money instanceof static) {
      return $money;
    }

    throw new LogicalException(sprintf('Only int|float|Money is supported, %s given.', gettype($money)));
  }

  public static function ofInt(int $money): self {
    return new static($money * 100);
  }

  public static function ofFloat(float $money): self {
    if ($money !== round($money, 2)) {
      throw new LogicalException('The price must be a maximum of two valid decimal numbers.');
    }

    return new static((int) ($money * 100));
  }

  public static function ofCents(int $money): self {
    return new static($money);
  }

  public function get(): int {
    return $this->value;
  }

  public function getReal(): float {
    return $this->value / 100;
  }

}

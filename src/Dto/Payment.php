<?php declare(strict_types=1);

namespace Drupal\comgate\Dto;

use Drupal\comgate\Dto\Codes\CurrencyCode;

/**
 * Comgate Payment DTO.
 */
class Payment extends Transaction {

  /**
   * Price for a product in cents or pennies
   *
   * Must be in minimum of 1 CZK; 0,1 EUR; 1 PLN; 100 HUF; 1 USD; 1 GBP; 5 RON; 1 HRK; 0,5 NOK; 0,5 SEK.
   */
  protected ?Money $price;

  /**
   * Currency code according to ISO 4217. Available currencies: CZK, EUR, PLN, HUF, USD, GBP, RON, HRK, NOK, SEK.
   */
  protected ?string $currency = CurrencyCode::CZK;

  /**
   * Short description of the product (1-16 characters).
   *
   * This item enables to filter payments in the Client Portal.
   */
  protected ?string $label;

  /**
   * Parameter is suitable for entering a variable symbol or order number on the Client's side
   * (it does not have to be unique, ie it is possible to create more payments with the same refId).
   *
   * In the Client Portal and daily csv. the parameter is marked as Client ID.
   */
  protected ?string $referenceId;

  /**
   * Contact email on the Payer (for the purposes of a possible complaint).
   */
  protected ?string $email;

  /**
   * The methods of payment from the table of payment methods, the value "ALL" if the method is to be chosen by the
   * payer.
   *
   * @var string[]
   */
  protected array $allowedMethods = [];

  /**
   * The methods of payment to exclude from the table of payment method.
   *
   * @var string[]
   */
  protected array $excludedMethods = [];

  /**
   * In the case of creating a payment in the background fill in “true“.
   *
   * When creating payment by redirection, fill in either "false" or do not use the parameter. You can find which
   * payment method is set up as allowed in the Client Portal in the Integration section - connection of e-shop.
   */
  protected bool $prepareOnly = TRUE;

  /**
   * Country of the customer.
   *
   * Possible values: ALL, AT, BE, CY, CZ, DE, EE, EL, ES, FI, FR, GB, HR, HU, IE, IT, LT, LU, LV, MT, NL, NO, PL, PT,
   * RO, SL, SK, SV, US. If the parameter is missing, "CZ" is used automatically. The parameter is used to limit the
   * selection of payment methods at the payment gateway. It is necessary to select the correct combination of
   * "country" and "curr" parameters for the given region. For example, to display Czech buttons and pay by card in
   * CZK, choose the combination country = CZ and curr = CZK.
   *
   * For Slovak bank buttons and card payments in EUR, select country = SK and curr = EUR.
   *
   * For Polish bank buttons and card payment in PLN, select country = PL and curr = PLN.
   *
   * For other foreign currencies, you can use the country = ALL parameter or another country code that the payment
   * gateway accepts.
   */
  protected ?string $country = NULL;

  /**
   * Identifier of the Client's bank account to which Comgate Payments will transfer money.
   *
   * If you do not fill in the parameter, the default Client account will be used. You can find a list of the Client's
   * accounts at https://portal.comgate.cz/.
   */
  protected ?string $account = NULL;

  /**
   * Contact telephone number on the Payer (for the purposes of a possible complaint).
   */
  protected ?string $phone = NULL;

  /**
   * Product identifier - this item is located in the client's daily csv under the name Product.
   */
  protected ?string $name = NULL;

  /**
   * Payer's language.
   *
   * Language code (ISO 639-1) in which the Payer will be shown instructions for completing the payment default
   * allowed values ("cs", "sk", "en", "pl", "fr", "ro", "de" , "hu", "si", "hr"), if the parameter is missing, "cs"
   * will be used.
   */
  protected ?string $lang = NULL;

  /**
   * Card payment pre-authorization.
   *
   * In the case of a request to pre-authorize credit card payments set to "true". In the case of a normal payment,
   * fill in "false" or do not use the parameter. Only for card payments.
   */
  protected ?bool $preauth = NULL;

  /**
   * Parameter for creating an initial transaction for recurring payments.
   * Only for Clients who have the service enabled.
   */
  protected ?bool $initRecurring = NULL;

  /**
   * Verification payment parameter, in case of a request to create a verification payment (value "true") it is not
   * necessary to send the initRecurring parameter.
   */
  protected ?bool $verification = NULL;

  /**
   * If you use the standard redirection of the payer to the payment gateway, do not fill in the parameter or enter the
   * value "false". The parameter is used only if you have implemented the display of the Comgate payment gateway in
   * the iframe in your e-shop. A value of "true" ensures that the gateway is displayed correctly in the iframe. This
   * can only be used for card payments. You can find more detailed information in the Payment Gateway section in the
   * e-shop (iframe).
   */
  protected ?bool $embedded = NULL;

  /**
   * Parameter of sending data to EET
   *
   * If filled in, it overloads the EET settings in the store configuration in the Client Portal.
   */
  protected ?bool $eetReport = NULL;

  /**
   * Structure with data for registration of payment to EET.
   *
   * Settings from the configuration in the Client Portal.
   */
  protected ?array $eetData = [];

  private function __construct() {
    parent::__construct();
  }

  public static function create(): self {
    return new static();
  }

  public function withRedirect(): self {
    $this->withPrepareOnly(TRUE);

    return $this;
  }

  public function withIframe(): self {
    $this->withPrepareOnly(TRUE);
    $this->withEmbedded(TRUE);

    return $this;
  }

  public function getPrice(): Money {
    return $this->price;
  }

  public function withPrice(float|Money|int $price): self {
    $this->price = Money::of($price);

    return $this;
  }

  public function getCurrency(): string {
    return $this->currency;
  }

  public function withCurrency(string $currency): self {
    $this->currency = $currency;

    return $this;
  }

  public function getLabel(): string {
    return $this->label;
  }

  public function withLabel(string $label): self {
    $this->label = $label;

    return $this;
  }

  public function getReferenceId(): string {
    return $this->referenceId;
  }

  public function withReferenceId(string $referenceId): self {
    $this->referenceId = $referenceId;

    return $this;
  }

  public function getEmail(): string {
    return $this->email;
  }

  public function withEmail(string $email): self {
    $this->email = $email;

    return $this;
  }

  /**
   * @return string[]
   */
  public function getAllowedMethods(): array {
    return $this->allowedMethods;
  }

  /**
   * @return string[]
   */
  public function getExcludedMethods(): array {
    return $this->excludedMethods;
  }

  public function withMethod(string $method): self {
    $this->allowedMethods[] = $method;

    return $this;
  }

  public function withoutMethod(string $method): self {
    $this->excludedMethods[] = $method;

    return $this;
  }

  public function getCountry(): ?string {
    return $this->country;
  }

  public function withCountry(string $country): self {
    $this->country = $country;

    return $this;
  }

  public function getAccount(): ?string {
    return $this->account;
  }

  public function withAccount(string $account): self {
    $this->account = $account;

    return $this;
  }

  public function getPhone(): ?string {
    return $this->phone;
  }

  public function withPhone(string $phone): self {
    $this->phone = $phone;

    return $this;
  }

  public function getName(): ?string {
    return $this->name;
  }

  public function withName(string $name): self {
    $this->name = $name;

    return $this;
  }

  public function getLang(): ?string {
    return $this->lang;
  }

  public function withLang(string $lang): self {
    $this->lang = $lang;

    return $this;
  }

  public function isPrepareOnly(): bool {
    return $this->prepareOnly;
  }

  public function withPrepareOnly(bool $prepareOnly): self {
    $this->prepareOnly = $prepareOnly;

    return $this;
  }

  public function isPreauth(): ?bool {
    return $this->preauth;
  }

  public function withPreauth(bool $preauth): self {
    $this->preauth = $preauth;

    return $this;
  }

  public function isInitRecurring(): ?bool {
    return $this->initRecurring;
  }

  public function withInitRecurring(bool $initRecurring): self {
    $this->initRecurring = $initRecurring;

    return $this;
  }

  public function isVerification(): ?bool {
    return $this->verification;
  }

  public function withVerification(bool $verification): self {
    $this->verification = $verification;

    return $this;
  }

  public function isEmbedded(): ?bool {
    return $this->embedded;
  }

  public function withEmbedded(bool $embedded): self {
    $this->embedded = $embedded;

    return $this;
  }

  public function isEetReport(): ?bool {
    return $this->eetReport;
  }

  public function withEetReport(bool $eetReport): self {
    $this->eetReport = $eetReport;

    return $this;
  }

  public function getEetData(): ?array {
    return $this->eetData;
  }

  public function withEetData(array $eetData): self {
    $this->eetData = $eetData;

    return $this;
  }

}

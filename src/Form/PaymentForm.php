<?php declare(strict_types=1);

namespace Drupal\comgate\Form;

use Drupal\comgate\ComgateService;
use Drupal\comgate\Dto\Payment;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form providing a payment button. Once clicked a user is redirected to Comgate.
 *
 * The form expects one argument containing the payment details.
 */
class PaymentForm extends FormBase {

  protected ComgateService $comgateService;

  protected ?Payment $payment;

  /**
   * OrderMessageActionsForm constructor.
   *
   * @param \Drupal\comgate\ComgateService $comgateService
   *   The Comgate service.
   */
  public function __construct(ComgateService $comgateService) {
    $this->comgateService = $comgateService;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('comgate'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'comgate_payment';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if (!count($form_state->getBuildInfo()['args'])) {
      throw new \InvalidArgumentException('Form "payment" argument missing.');
    }

    $this->payment = $form_state->getBuildInfo()['args'][0];

    //    // Workaround for https://www.drupal.org/project/drupal/issues/2821852
    //    // Otherwise $form_state clashes with other instances of this form.
    //    $form_state->setRequestMethod('POST');
    //    $form_state->setCached(TRUE);

    $form['actions'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['actions']],
    ];
    $form['actions']['pay'] = [
      '#name' => uniqid('pay_'),
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Pay'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $triggering_element = $form_state->getTriggeringElement();
    switch ($triggering_element['#id']) {
      case 'edit-pay':
        $this->pay($form_state);
        break;
    }
  }

  /**
   * Creates a payment and redirects the user to Comgate.
   */
  protected function pay(FormStateInterface $form_state): void {
    $redirect = $this->comgateService->createPayment($this->payment);
    $response = new TrustedRedirectResponse(Url::fromUri($redirect->getUrl())->toString());
    $metadata = $response->getCacheableMetadata();
    $metadata->setCacheMaxAge(0);
    $form_state->setResponse($response);
  }

}

<?php declare(strict_types=1);

namespace Drupal\comgate;

use Drupal\comgate\Http\ITransport;
use Drupal\comgate\Http\Transport;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use Psr\Log\LoggerInterface;

/**
 * Comgate gateway representation and entrypoint.
 */
class Comgate {

  private Config $config;

  private array $middlewares = [];

  private ?LoggerInterface $logger = NULL;

  /**
   * Creates a new Comgate instance with a given config options.
   *
   * @param \Drupal\comgate\Config|null $config
   *   The Comgate configuration, by default taken from Settings (settings.php).
   */
  private function __construct(?Config $config = NULL) {
    $this->config = $config ?? Config::get();
  }

  /**
   * Creates a new Comgate instance with a given config options.
   *
   * @param \Drupal\comgate\Config|null $config
   *   The Comgate configuration, by default taken from Settings (settings.php).
   */
  public static function defaults(?Config $config = NULL): self {
    return new static($config);
  }

  /**
   * Creates a new Comgate testing instance with a given config options.
   *
   * @param \Drupal\comgate\Config|null $config
   *   The Comgate configuration, by default taken from Settings (settings.php).
   */
  public static function testing(?Config $config = NULL): self {
    return self::defaults($config)->withTest();
  }

  public function withTest(bool $test = TRUE): self {
    $this->config->setTest($test);

    return $this;
  }

  public function withMiddleware(callable $middleware): self {
    $this->middlewares[] = $middleware;

    return $this;
  }

  public function withLogger(LoggerInterface $logger): self {
    $this->logger = $logger;

    return $this;
  }

  public function createClient(): ComgateClient {
    return new ComgateClient($this->createTransport());
  }

  protected function createGuzzle(?callable $handler = NULL): ClientInterface {
    $handlers = HandlerStack::create($handler);

    foreach ($this->middlewares as $middleware) {
      $handlers->push($middleware);
    }

    if ($this->logger !== NULL) {
      $handlers->push(Middleware::log($this->logger, new MessageFormatter(MessageFormatter::DEBUG)));
    }

    return new GuzzleClient([
      'handler' => $handlers,
      'base_uri' => Config::COMGATE_URL,
      'headers' => [
        'User-Agent' => Config::USER_AGENT,
      ],
    ]);
  }

  protected function createTransport(): ITransport {
    return new Transport($this->createGuzzle(), $this->config);
  }

}

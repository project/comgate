<?php declare(strict_types=1);

namespace Drupal\comgate\Exception\Runtime;

use Drupal\comgate\Exception\RuntimeException;

class ComgateException extends RuntimeException {

}

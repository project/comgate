<?php declare(strict_types=1);

namespace Drupal\comgate\Exception;

use LogicException;

class LogicalException extends LogicException {

}

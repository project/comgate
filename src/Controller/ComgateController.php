<?php declare(strict_types=1);

namespace Drupal\comgate\Controller;

use Drupal\comgate\ComgateService;
use Drupal\comgate\Config;
use Drupal\comgate\Dto\Codes\PaymentStatusCode;
use Drupal\comgate\Dto\PaymentStatus;
use Drupal\comgate\Dto\Transaction;
use Drupal\comgate\Exception\Runtime\ComgateException;
use Drupal\comgate\Exception\RuntimeException;
use Drupal\comgate\FloodControlTrait;
use Drupal\Core\Controller\ControllerBase;
use Drupal\user\UserFloodControlInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\IpUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Comgate controller.
 */
class ComgateController extends ControllerBase {

  use FloodControlTrait;

  protected ComgateService $comgateService;

  protected Config $config;

  protected LoggerInterface $logger;

  /**
   * Comgate controller constructor.
   *
   * @param \Drupal\comgate\ComgateService $comgate_service
   *   Comgate service.
   * @param \Drupal\comgate\Config $config
   *   Comgate config.
   * @param \Drupal\user\UserFloodControlInterface $floodControl
   *   Flood control service.
   */
  public function __construct(ComgateService $comgate_service, Config $config, UserFloodControlInterface $floodControl) {
    $this->comgateService = $comgate_service;
    $this->config = $config;
    $this->logger = $this->getLogger('comgate');
    $this->floodControl = $floodControl;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('comgate'),
      Config::get(),
      $container->get('user.flood_control'),
    );
  }

  /**
   * Handler for background payment results.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function resultCallback(Request $request): Response {
    $this->floodControl('comgate.result', 600);

    if (!$this->validateComgateRequest($request)) {
      $this->logger->error('Incoming payment result processing failed: request invalid.');
      return Response::create('Request invalid.', Response::HTTP_BAD_REQUEST);
    }

    try {
      $this->comgateService->handlePaymentResult($request->request->all());
    } catch (RuntimeException $e) {
      $this->logger->error('Incoming payment result processing failed: %message', [
        '%message' => $e->getMessage(),
      ]);
      return Response::create('Request invalid.', Response::HTTP_BAD_REQUEST);
    }

    return Response::create();
  }

  /**
   * Handler for customer return page.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return array|\Symfony\Component\HttpFoundation\Response
   *   The response or renderable markup.
   */
  public function returnPage(Request $request): array|Response {
    $this->floodControl('comgate.return', 600);

    // Validate transaction ID.
    $transactionId = $request->get('id');
    $this->logger->debug('Received request with transaction id @id.', [
      '@id' => $transactionId,
    ]);

    if (!is_string($transactionId) || empty($transactionId)) {
      $this->logger->error('Return page request invalid: id parameter missing.');
      return $this->handleValidationError();
    }

    // Get the payment status for this transaction from Comgate.
    try {
      $status = $this->comgateService->getPaymentStatus(Transaction::existing($transactionId));
    } catch (ComgateException $e) {
      $this->logger->error('Return page request invalid: %message', ['%message' => $e->getMessage()]);
      return $this->handleComgateException($e);
    }

    $this->logger->debug('Fetched payment status with transaction id @id and ref @ref.', [
      '@id' => $status->getTransactionId(),
      '@ref' => $status->getReferenceId(),
    ]);

    return $this->buildReturnPageOutput($status);
  }

  /**
   * Builds the user output based on the order status.
   *
   * @param \Drupal\comgate\Dto\PaymentStatus $status
   *   The payment status.
   *
   * @return array|\Symfony\Component\HttpFoundation\Response
   *   The renderable output.
   */
  protected function buildReturnPageOutput(PaymentStatus $status): array|Response {
    $output = '';
    $localized_status = $this->t(strtolower($status->getStatus()));
    switch ($status->getStatus()) {
      case PaymentStatusCode::AUTHORIZED:
      case PaymentStatusCode::PAID:
        $output .= '<h3>' . t('Thank you!') . '</h3>';
        $output .= '<p>' . t('The current status of your payment is: <strong>%status</strong>', ['%status' => $localized_status]) . '</p>';
        break;

      case PaymentStatusCode::PENDING:
        $output .= '<h3>' . t('Thank you!') . '</h3>';
        $output .= '<p>' . t('The current status of your payment is: <strong>%status</strong>', ['%status' => $localized_status]) . '</p>';
        $output .= '<p>' . t(
            'We are awaiting a confirmation from the payment gate and will update the status once the transaction is processed.' .
            ' This might take some time depending on your bank.',
          ) . '</p>';
        break;

      default:
        $output .= '<h3>' . t('Something went wrong?') . '</h3>';
        $output .= '<p>' . t('The current status of your payment is: <strong>%status</strong>', ['%status' => $localized_status]) . '</p>';
        $output .= '<p>' . t('Need help? Feel free to <a href="mailto:@email">contact us</a>.', [
            '@email' => $this->config('system.site')->get('mail'),
          ]) . '</p>';
        break;
    }

    return [
      '#markup' => $output,
    ];
  }

  /**
   * Validates incoming Comgate request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The Comgate request to validate.
   */
  protected function validateComgateRequest(Request $request): bool {
    return $request->isSecure() &&
      IpUtils::checkIp($request->getClientIp(), $this->config->getIpRange()) &&
      $request->request->get('secret') === $this->config->getSecret();
  }


  /**
   * Handles a situation when the request is invalid (e.g. missing a required parameter).
   *
   * @return array|\Symfony\Component\HttpFoundation\Response
   *  The response to show the user.
   */
  protected function handleValidationError(): array|Response {
    throw new NotFoundHttpException();
  }

  /**
   * Handles a given ComGate exception.
   *
   * @param \Drupal\comgate\Exception\Runtime\ComgateException $e
   *  The exception to handle.
   *
   * @return array|\Symfony\Component\HttpFoundation\Response
   *  The response to show the user.
   */
  protected function handleComgateException(ComgateException $e): array|Response {
    throw new NotFoundHttpException($e->getMessage());
  }
}

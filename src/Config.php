<?php declare(strict_types=1);

namespace Drupal\comgate;

use Drupal\comgate\Exception\LogicalException;
use Drupal\Core\Site\Settings;

/**
 * Comgate configuration.
 */
class Config {

  public const COMGATE_URL = 'https://payments.comgate.cz/v1.0/';

  public const COMGATE_IP_RANGE = '89.185.236.55/32';

  public const USER_AGENT = 'DrupalComgate/v1/Guzzle/6';

  private string $merchant;

  private string $secret;

  private bool $test;

  private string $ipRange;

  public function __construct(string $merchant, string $secret, bool $test = FALSE, string $ipRange = self::COMGATE_IP_RANGE) {
    $this->merchant = $merchant;
    $this->secret = $secret;
    $this->test = $test;
    $this->ipRange = $ipRange;
  }

  /**
   * Creates a new Config instance with properties taken from Drupal Settings (settings.php).
   *
   * @throws \Drupal\comgate\Exception\LogicalException
   *   If mandatory settings properties are not found.
   */
  public static function get(): self {
    $settings = Settings::get('comgate', []);
    foreach (['merchant', 'secret'] as $property) {
      if (!isset($settings[$property])) {
        throw new LogicalException('Missing $settings[\'comgate\'][\'' . $property . '\'] in settings.php.');
      }
    }

    return new static(
      $settings['merchant'],
      $settings['secret'],
      filter_var($settings['test'], FILTER_VALIDATE_BOOLEAN),
      $settings['ip_range'] ?? self::COMGATE_IP_RANGE,
    );
  }

  public function getMerchant(): string {
    return $this->merchant;
  }

  public function getSecret(): string {
    return $this->secret;
  }

  public function isTest(): bool {
    return $this->test;
  }

  public function setTest(bool $test): self {
    $this->test = $test;
    return $this;
  }

  public function getIpRange(): string {
    return $this->ipRange;
  }

  public function setIpRange(string $ip_range): self {
    $this->ipRange = $ip_range;
    return $this;
  }

}

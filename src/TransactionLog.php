<?php declare(strict_types=1);

namespace Drupal\comgate;

use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\State\StateInterface;

/**
 * Log of Comgate transactions.
 *
 * Provides additional protection against possibly forged payment requests.
 *
 * The log is backed by an persistent array in a global state
 * and thus synchronized using a locking mechanism.
 *
 * Every log entry is an associative array item whose key is a Comgate
 * transaction ID and the value is a time of the entry creation.
 */
class TransactionLog {

  /**
   * Name of the persistent state variable containing the log.
   */
  protected const TRANSACTION_LOG_NAME = 'comgate.transaction_log';

  /**
   * Default transaction life span (14 days).
   */
  protected const DEFAULT_TRANSACTION_TTL = 14 * 86400;

  protected int $transactionTTL = self::DEFAULT_TRANSACTION_TTL;

  protected StateInterface $state;

  protected LockBackendInterface $lock;

  /**
   * ComgateService constructor.
   */
  public function __construct(StateInterface $state, LockBackendInterface $lock) {
    $this->state = $state;
    $this->lock = $lock;
  }

  /**
   * Stores a given transaction ID to the log.
   *
   * @param string $transactionId
   *   The transaction ID to remember.
   */
  public function remember(string $transactionId): void {
    $this->acquireLock(self::TRANSACTION_LOG_NAME);

    // Purge old log entries.
    $this->forgetOlderThan(time() - $this->transactionTTL);

    // Log the new transaction.
    $log = $this->getValue();
    $log[$transactionId] = time();
    $this->setTransactionLog($log);

    $this->releaseLock(self::TRANSACTION_LOG_NAME);
  }

  /**
   * Checks if a given transaction ID exists in the log.
   *
   * @param string $transactionId
   *   The transaction ID to look up.
   *
   * @return bool
   *   True if the transaction ID exists in the log.
   */
  public function canRecall(string $transactionId): bool {
    $log = $this->getValue();
    return isset($log[$transactionId]);
  }

  /**
   * Purges any transactions from the log older than a given timestamp.
   *
   * @param int $timestamp
   *   The least time of any transaction in the log.
   */
  protected function forgetOlderThan(int $timestamp): void {
    $log = $this->getValue();
    foreach ($log as $transactionId => $transactionTime) {
      if ($transactionTime < $timestamp) {
        unset($log[$transactionId]);
      }
    }
    $this->setTransactionLog($log);
  }

  protected function getValue(): array {
    $this->state->resetCache();
    return $this->state->get(self::TRANSACTION_LOG_NAME, []);
  }

  protected function setTransactionLog(array $log): void {
    $this->state->set(self::TRANSACTION_LOG_NAME, $log);
  }

  protected function acquireLock(string $name): bool {
    $lock_attempt = 0;

    while (!$this->lock->acquire($name)) {
      $this->lock->wait($name, 3);
      $lock_attempt++;
      if ($lock_attempt >= 3) {
        return FALSE;
      }
    }

    return TRUE;
  }

  protected function releaseLock(string $name): void {
    $this->lock->release($name);
  }

}

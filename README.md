<strong>Basic integration of a Czech payment gateway <a href="https://comgate.cz">Comgate</a>.</strong>

This module provides a simple API that allows calling Comgate methods for creating payments, handling returns from the gateway and reading payment statuses. Basically this is just a <a href="https://github.com/comgate-payments/sdk-php">Comgate PHP SDK</a> wrapped in a simple Drupal package (with the author's blessing).

<em>Please note that this module does not depend on Drupal Commerce intentionally and can be used without it.</em>

<h3 id="module-project--features">Features</h3>
<ul>
  <li>API for payment creation</li>
  <li>Controller for secure payment status handling</li>
  <li>Simple configuration using settings.php</li>
  <li>Necessary Drupal permissions only</li>
</ul>

<h3 id="module-project--post-installation">Post-Installation</h3>
After enabling the module, you should <strong>put following block to your <code>settings.php</code></strong> and make sure to <strong>grant the "use comgate" permission</strong>.

```php
/**
 * Comgate payments integration.
 */
if (getenv('COMGATE_MERCHANT')) {
  $settings['comgate'] = [
    'merchant' => getenv('COMGATE_MERCHANT'),
    'secret' => getenv('COMGATE_SECRET'),
    'test' => getenv('COMGATE_TEST'),
  ];
}
```

<em>*You will get the merchant and secret values in your Comgate account.</em>

<h3 id="module-project--development">Maintenance and development</h3>
The module is considered feature complete and supported by the author as it is being operated in production. Security is our main concern with this module.
In future we would like to remove the Comgate SDK code and use it as Composer dependency instead (contributions are welcome).

